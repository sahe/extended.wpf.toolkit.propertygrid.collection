# Extended.Wpf.Toolkit.PropertyGrid.Collection

Published via Nuget: https://www.nuget.org/packages/Extended.Wpf.Toolkit.PropertyGrid.Collection/

Copied from: https://stackoverflow.com/questions/36286530/xceed-wpf-propertygrid-show-item-for-expanded-collection

Background information: https://www.codeproject.com/Articles/4448/Customized-display-of-collection-data-in-a-Propert

## Howto design the Model 

1. Apply Attribute ```[ExpandableObject]```
2. Use ```ExpandableObservableCollection<T>```

Sample:

```C#

public class ComplexObject
{
    public int ID { get; set; }

    [ExpandableObject]
    public ExpandableObservableCollection<ComplexSubObject> Classes { get; set; }

    public ComplexObject()
    {
        ID = 1;
        Classes = new ExpandableObservableCollection<ComplexSubObject>();
        Classes.Add(new ComplexSubObject() { Name = "CustomFoo" });
        Classes.Add(new ComplexSubObject() { Name = "My Other Foo" });
    }
}

public class ComplexSubObject
{
    public string Name { get; set; }

    [ExpandableObject]
    public ExpandableObservableCollection<SimpleValues> Types { get; set; }

    public ComplexSubObject()
    {
        Types = new ExpandableObservableCollection<SimpleValues>();
        Types.Add(new SimpleValues() { name = "foo", value = "bar" });
        Types.Add(new SimpleValues() { name = "bar", value = "foo" });
    }
}

public class SimpleValues
{
    public string name { get; set; }

    public string value { get; set; }
}

```    