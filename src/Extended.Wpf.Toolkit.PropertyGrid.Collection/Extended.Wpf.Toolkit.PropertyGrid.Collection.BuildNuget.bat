echo off

REM navigate to current directory
cd /d %~dp0

REM take file name of batch without extension
set project=%~n0
REM replace ".BuildNuget" with ""
set project=%project:.BuildNuget=%
set nuget=..\..\tools\nuget.exe
set output=bin\Release
set deploy=S:\NuGet\

echo .
echo nuget pack %project% to %output%
echo .
echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo !!! Please double check:                                         !!!
echo !!!  - Compiled you project in Release?                          !!!
echo !!!  - Increased assembly version?                               !!!
echo !!!  - Created and pushed a git tag?                             !!!
echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo .

pause

%nuget% update -self
%nuget% pack %project%.csproj -Exclude FodyWeavers.xml -OutputDirectory %output% -Prop Configuration=Release

pause