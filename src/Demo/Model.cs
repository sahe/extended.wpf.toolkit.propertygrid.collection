﻿using System.ComponentModel;
using Extended.Wpf.Toolkit.PropertyGrid.Collection;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;

namespace Demo
{
    public class ComplexObject
    {
        public int ID { get; set; }

        [ExpandableObject]
        public ExpandableObservableCollection<ComplexSubObject> Classes { get; set; }
    }

    public class ComplexSubObject
    {
        public ComplexSubObject(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

        [ExpandableObject]
        public ExpandableObservableCollection<SimpleValues> Types { get; set; }
    }

    [ExpandableObject] // When implementing INotifyPropertyChanged, ExpandableObjectAttribute has to be set
    public class ChildComplexSubObject : ComplexSubObject, INotifyPropertyChanged
    {
        public ChildComplexSubObject(string name)
            : base(name)
        {
        }

        public int AdditionalProp { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class SimpleValues

    {
        public SimpleValues(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}