﻿using Extended.Wpf.Toolkit.PropertyGrid.Collection;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;

namespace Demo
{
    public class MainWindowViewModel
    {
        /// <summary> This the object we want to be able to edit in the data grid. </summary>
        [ExpandableObject]
        public ComplexObject BindingComplexObject { get; set; }

        public MainWindowViewModel()
        {
            BindingComplexObject = new ComplexObject()
            {
                ID = 1,
                Classes = new ExpandableObservableCollection<ComplexSubObject>
                {
                    new ComplexSubObject("blah")
                    {
                        Name = "CustomFoo",
                        Types = new ExpandableObservableCollection<SimpleValues>
                        {
                            new SimpleValues("blah") { Name = "foo", Value = "bar" },
                            new SimpleValues("blah") { Name = "bar", Value = "foo" },
                        },
                    },
                    new ChildComplexSubObject("blah")
                    {
                        Name = "My Other Foo",
                        AdditionalProp = 88,
                        Types = new ExpandableObservableCollection<SimpleValues>
                        {
                            new SimpleValues("blah") { Name = "foo", Value = "bar" },
                            new SimpleValues("blah") { Name = "bar", Value = "foo" },
                        },
                    },
                },
            };
        }
    }
}